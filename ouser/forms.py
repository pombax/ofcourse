from django import forms
from ouser.models import OUser


class KullaniciUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(KullaniciUpdateForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs["class"] = "form-control"
        self.fields['last_name'].widget.attrs["class"] = "form-control"
        self.fields['website'].widget.attrs["class"] = "form-control"
        self.fields['twitter'].widget.attrs["class"] = "form-control"
        self.fields['profile_pic'].widget.attrs["class"] = "form-control"


    class Meta:
        model = OUser
        fields = ['first_name', 'last_name', 'website','twitter','profile_pic']
