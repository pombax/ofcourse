from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,PermissionsMixin)
from django.utils import timezone
from django.contrib.auth.models import UserManager
from courses.models import Course

class CustomUserManager(UserManager):

    FIELDS = [
        'email', 'username', 'first_name', 'last_name', 'is_staff','is_active','is_student','is_instructor'
    ]

    def create_user(self, email,username, password,*args, **kwargs):
        if not email:
            raise ValueError('Email required')
        user = self.model(*args, **kwargs)
        user.email = email
        user.username = username
        user.set_password(password)
        user.save(using=self._db)
        return user


class OUser(AbstractBaseUser,PermissionsMixin):
    email = models.EmailField('E-Mail',unique=True)
    username = models.CharField('Kullanici Adi',unique=True, max_length=30)
    first_name = models.CharField('Adi',max_length=64,blank=True)
    last_name = models.CharField('Soyadi',max_length=64,blank=True)
    is_staff = models.BooleanField('Admin',default=False)
    is_active = models.BooleanField('Aktif', default=False)
    is_student = models.BooleanField('Ogrenci',default=True)
    is_instructor = models.BooleanField('Egitmen',default=False)
    date_joined = models.DateTimeField('Tarih',auto_now_add=True)
    courses = models.ManyToManyField(Course,blank=True,null=True)
    objects = CustomUserManager()
    website = models.URLField(blank=True,null=True)
    twitter = models.CharField('Twitter Kullanici Adi',max_length=255,blank=True,null=True)
    profile_pic = models.ImageField(upload_to='profile_pic/',default='covers/default.jpg')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        full_name = u'%s %s %s' % (self.last_name, self.first_name, self.middle_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
