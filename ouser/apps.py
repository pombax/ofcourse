from __future__ import unicode_literals
from django.apps import AppConfig


class OuserConfig(AppConfig):
    name = 'ouser'
