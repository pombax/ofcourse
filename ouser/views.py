from django.shortcuts import render
from django.views.generic import ListView,DetailView
from ouser.models import OUser
from django.contrib.auth.decorators import login_required
from .forms import KullaniciUpdateForm
from django.shortcuts import render, HttpResponseRedirect,HttpResponse
from django.core.exceptions import PermissionDenied
from django.core.context_processors import csrf


class KullaniciBilgileri(DetailView):
    context_object_name = "kullanici"
    template_name = "ouser/userprofile.html"

    def get_object(self):
        return OUser.objects.all().get(username__iexact=self.kwargs['username'])


@login_required
def kullanici_duzenle(request):
    username = request.user.username
    if request.user.username == username:
        if request.method == 'POST':
            user = OUser.objects.get(username=request.user.username)
            form = KullaniciUpdateForm(request.POST, request.FILES, instance=user)
            if form.is_valid():
                form.save()
                return HttpResponse('Profiliniz Guncellenmistir...')
        else:
            profile_instance = OUser.objects.get(username=request.user.username)
            form = KullaniciUpdateForm(instance=profile_instance)
        args = {}
        args.update(csrf(request))
        args['form'] = form
        args['this_user'] = OUser.objects.get(username=request.user.username)
        args['this_profile'] = OUser .objects.get(username=request.user.username)
        return render(request, 'ouser/updateprofile.html', args)
    else:
	    return HttpResponse(content="Forbidden", status=403)
