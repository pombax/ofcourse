from django import forms


class SearchForm(forms.Form):
    keyword = forms.CharField()

    def clean_keyword(self):
        word = self.cleaned_data['keyword']
        if len(word)<3:
            raise forms.ValidationErro('Aranacak kelime 3 harften fazla olmalidir')
        return word
