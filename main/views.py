from django.shortcuts import render

def gizlilik_politikasi(request):
    return render(request, 'main/gizlilik_politikasi.html')

def destek(request):
    return render(request, 'main/destek.html')

def hakkimizda(request):
    return render(request, 'main/hakkimizda.html')

def iletisim(request):
    return render(request, 'main/iletisim.html')
