from django import forms
from ouser.models import OUser
from django.http import HttpResponse
from django.contrib.auth import get_user_model, authenticate


class RegistrationForm(forms.ModelForm):
    email = forms.EmailField()
    username = forms.CharField()
    password1 = forms.CharField(widget=forms.PasswordInput,label="Parola")
    password2 = forms.CharField(widget=forms.PasswordInput,label="Parola Tekrar")

    def clean_email(self):
        email = self.cleaned_data['email']
        user_model = get_user_model()
        try:
            OUser.objects.get(email=email)
            raise forms.ValidationError("Bu mail adresiyle kayitli uye bulunmakta")
        except user_model.DoesNotExist:
            pass

        return email

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get("password1")
        confirm_password = cleaned_data.get("password2")
        if password != confirm_password:
            raise forms.ValidationError("Parolalar eslesmiyor")

        return cleaned_data

    def save(self,request ,*args, **kwargs):
        email = self.cleaned_data['email']
        password = self.cleaned_data['password1']
        username = self.cleaned_data['username']
        user = OUser.objects.create_user(email=email, username=username,password=password)
        user.save()
        return user

    class Meta:
        model = OUser
        fields = ['username','email','password1','password2']
