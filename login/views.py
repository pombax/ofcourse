from django.shortcuts import render,redirect,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from .forms import RegistrationForm
from ouser.models import OUser
from courses.models import Course
from django.views.generic import CreateView,DetailView
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login


class HomePageView(DetailView):
    context_object_name = "populerkurslar"
    template_name = "login/index.html"

    def get_object(self):
        return Course.objects.all().filter(course_isActive=True)[:8]


class RegistrationView(CreateView):
    form_class = RegistrationForm
    model = OUser
    template_name = "login/register.html"

    def form_valid(self, form):
        obj = form.save(request=self.request)
        opts = {
            'use_https': self.request.is_secure(),
            'email_template_name': 'registration/verification.html',
            'subject_template_name': 'registration/verification_subject.txt',
            'request': self.request,
        }
        return HttpResponse('Uyelik kaydiniz alinmistir, uyeliginiz onaylandiktan sonra giris yapabilirsiniz...')


def login(request):
    state = 'ii'
    username = password = ''
    if request.POST:
        username = request.POST.get('l_username')
        password = request.POST.get('l_password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                state = "Basariyla giris yaptiniz!"
                return HttpResponseRedirect('/')
            else:
                return HttpResponse("Hesabiniz aktif degil, yoneticiyle iletisime gecin.")
        else:
            state = "Kullanici adi veya sifreniz yanlis."
            return HttpResponse("Gecersiz Giris denemesi.")
    return render_to_response('login/login.html',{'state':state, 'username': username})