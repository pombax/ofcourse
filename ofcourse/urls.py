from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns, include, url
from login.views import RegistrationView,login,HomePageView
from django.contrib.auth import views
from courses.views import CourseContent,CourseListView,CategoryListView,enrollCourse,IcerikleriGoruntule,IcerikGoruntule,kurstanayril
from instructor.views import InstructorPanel,CreateCourseView,UpdateCourseView,IcerikEkleView
from courses.models import Course
from ouser.views import KullaniciBilgileri, kullanici_duzenle

from forum.views import BaslikAc,QA,SoruDetay
from main.views import gizlilik_politikasi, destek, hakkimizda, iletisim

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomePageView.as_view()),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('',
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve',{'document_root':settings.STATIC_ROOT}),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root':settings.MEDIA_URL}),
    (r'^search/', include('haystack.urls')),
)

urlpatterns += [
    url(r'^register/$', RegistrationView.as_view(), name='register'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$','django.contrib.auth.views.logout',name='logout',kwargs={'next_page': '/'}),

    url(r'^kurs/(?P<course_ID>\d+)/$',CourseContent.as_view(), name='slug'),
    url(r'^kurs/(?P<course_ID>\d+)/icerik/$',IcerikleriGoruntule.as_view(), name='icerik'),
    url(r'^kurs/(?P<course_ID>\d+)/icerik/sil/$', kurstanayril),
    url(r'^kurs/(?P<course_ID>\d+)/icerik/(?P<slug>[\w-]+)/$', IcerikGoruntule.as_view()),
    url(r'^kurs/(?P<course_ID>\d+)/enroll/$',enrollCourse, name='enroll'),
    url(r'^kurs/(?P<course_ID>\d+)/forum/baslikac/$',BaslikAc.as_view() ),
    url(r'^kurs/(?P<course_ID>\d+)/forum/$', QA.as_view()),
    url(r'^kurs/(?P<course_ID>\d+)/forum/(?P<question_ID>\d+)/$', SoruDetay.as_view(),name="cevapyolla"),


    url(r'^kursduzenle/(?P<pk>\d+)/$',UpdateCourseView.as_view(), name='kursduzenle'),
    url(r'^kursduzenle/(?P<pk>\d+)/icerikekle/$',IcerikEkleView.as_view()),


    url(r'^kurslar/$', CourseListView.as_view() ,name='kurslar'),
    url(r'^kullanici/(?P<username>.*)/$', KullaniciBilgileri.as_view() ,name='username'),
    url(r'^profil-duzenle/$', kullanici_duzenle),

    url(r'^egitmen/$', InstructorPanel.as_view() ,name='egitmen'),
    url(r'^kursolustur/$', CreateCourseView.as_view() ,name='kursolustur'),


    # statik sayfalar

    url(r'gizlilik-politikasi/$', gizlilik_politikasi),
    url(r'destek/$', destek),
    url(r'hakkimizda/$', hakkimizda),
    url(r'iletisim/$', iletisim),

]
