# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-14 14:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instructor', '0011_auto_20160514_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='lecture',
            name='slug',
            field=models.SlugField(blank=True, null=True),
        ),
    ]
