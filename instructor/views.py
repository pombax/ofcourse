from .models import Instructor
from courses.models import Course
from django.views.generic import DetailView,CreateView,ListView,UpdateView
from courses.forms import CreateCourseForm
from instructor.forms import UpdateCourseForm, Icerik_Ekleme_Form
from django.http import HttpResponse
from ouser.models import OUser
from .models import Lecture
from django.shortcuts import render_to_response, get_object_or_404


class InstructorPanel(DetailView):
    context_object_name = "icerik"
    template_name = "instructor/panel.html"

    def get_object(self):
       try:
            return Instructor.objects.get(user=self.request.user)
       except:
            return 0

    def get_context_data(self, **kwargs):
        context = super(InstructorPanel,self).get_context_data(**kwargs)
        context['kurs'] = Instructor.objects.all().filter(user=self.request.user)
        context['kurs_sayisi'] = Course.objects.all().filter(courseInstructor=self.get_object())
        context['egitmen_kurslar'] = Course.objects.all().filter(courseInstructor=self.get_object())
        return context


class CreateCourseView(CreateView):
    form_class = CreateCourseForm
    model = Course
    template_name = "courses/createcourse.html"

    def cozmen(self):
        a = get_object_or_404(Instructor.objects.filter(user=self.request.user))
        print(a)

    def form_valid(self, form):
        obj = form.save(commit=False)
        userX = get_object_or_404(OUser.objects.filter(username=self.request.user.username))
        userX.is_instructor = True
        userX.save()
        b = Instructor.objects.filter(user=userX)       #kod yapisi duzeltilmeli
        if len(b) == 0:
            c = Instructor.objects.create(user=userX)
            obj.courseInstructor = c
        elif len(b) > 0:
            obj.courseInstructor = Instructor.objects.get(user=userX)
        obj.save()
        opts = {
            'request': self.request,
        }
        return HttpResponse('Kurs basvuru kaydiniz alinmistir')


class UpdateCourseView(UpdateView):
    form_class = UpdateCourseForm
    model = Course
    template_name = "courses/updatecourse.html"
    success_url ="/"
    context_object_name = "icerik"

    def get_object(self):
        return Course.objects.get(courseID=self.kwargs['pk'])


class IcerikEkleView(UpdateView):
    form_class = Icerik_Ekleme_Form
    template_name = "courses/icerikekle.html"
    success_url ="/"
    context_object_name = "icerik"

    def form_valid(self, form):
        obj = form.save(commit=False)
        kurs = Course.objects.get(courseID=self.kwargs['pk'])
        obj.course = kurs
        obj.save()
        return HttpResponse('Icerik Eklenmistir...')

    def get_object(self):
        return Course.objects.get(courseID=self.kwargs['pk'])
