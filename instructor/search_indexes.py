import datetime
from haystack import indexes
from .models import Instructor


class CourseIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    user = indexes.CharField(model_attr='user')


    def get_model(self):
        return Instructor

