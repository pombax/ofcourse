from django.contrib import admin
from .models import Instructor, Section, Lecture


class InstructorAdmin(admin.ModelAdmin):
    fields = ['user','biography','profile_pic']


class SectionAdmin(admin.ModelAdmin):
    fields = ['course','name','sectionno']


class LectureAdmin(admin.ModelAdmin):
    fields = ['name','video','course','slug']


admin.site.register(Instructor,InstructorAdmin)
admin.site.register(Lecture,LectureAdmin)
admin.site.register(Section,SectionAdmin)
