from django import forms
from courses.models import Course, CourseLanguage, Category, CourseLevels
from .models import Lecture
from django.utils import timezone
from django.http import HttpResponse
from haystack.forms import SearchForm

class UpdateCourseForm(forms.ModelForm):
    courseName = forms.CharField()
    courseDesc = forms.CharField()
    courseCoverPhoto = forms.ImageField()
    coursePreVideo = forms.URLField()
    courseLevel = forms.ModelChoiceField(queryset=CourseLevels.objects.all(), widget=forms.Select)
    courseLanguage = forms.ModelChoiceField(queryset=CourseLanguage.objects.all(), widget=forms.Select)
    courseCategory = forms.ModelChoiceField(queryset=Category.objects.all(), widget=forms.Select)

    class Meta:
        model = Course
        fields = ['courseName','courseDesc','courseCoverPhoto','coursePreVideo','courseLevel','courseLanguage',
                  'courseCategory', ]


class Icerik_Ekleme_Form(forms.ModelForm):

    class Meta:
        model = Lecture
        fields = ['name','video']

