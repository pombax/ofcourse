from django.db import models
from ouser.models import OUser
from embed_video.fields import EmbedVideoField
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify


class Instructor(models.Model):
    user = models.OneToOneField(OUser)
    biography = models.TextField()
    profile_pic = models.ImageField(upload_to='profile_pic/')

    def __str__(self):
        return self.user

    def __unicode__(self):
        return "{}".format(unicode(self.user))


@python_2_unicode_compatible
class Section(models.Model):
    course = models.ForeignKey('courses.Course')
    name = models.CharField(max_length=230)
    sectionno = models.IntegerField(default=1)

    def __unicode__(self):
        return "Kurs:{} - Bolum:{}".format(unicode(self.name),unicode(self.course.courseName))

    def __str__(self):
        return self.name


class Lecture(models.Model):
    name = models.CharField(max_length=20,unique=True)
    video = models.CharField(max_length=200)
    course = models.ForeignKey('courses.Course')
    slug = models.SlugField(null=True,blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Lecture, self).save(*args, **kwargs)


    @property
    def lecture_id(self):
        return lecture_id

    def __str__(self):
        return self.name

    def __unicode__(self):
        return (self.course.courseName + " - " + self.name)
