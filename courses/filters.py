# -*- coding: utf-8 -*-

import django_filters
from .models import Course


class CourseFilter(django_filters.FilterSet):
    courseName = django_filters.CharFilter(label="Kurs Adı",lookup_expr='icontains')

    class Meta:
        model = Course
        fields = ['courseLanguage','courseLevel','courseCategory',]

