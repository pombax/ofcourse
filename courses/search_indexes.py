import datetime
from haystack import indexes
from .models import Course


class CourseIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    courseName = indexes.CharField(model_attr='courseName')
    courseStartDate = indexes.DateTimeField(model_attr='courseStartDate')
    courseID = indexes.CharField(model_attr='courseID')


    def get_model(self):
        return Course

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(courseStartDate__lte=datetime.datetime.now())
