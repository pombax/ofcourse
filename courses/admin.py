from django.contrib import admin
from .models import Category,Course,CourseLevels,CourseLanguage


class CategoryAdmin(admin.ModelAdmin):    # kategorilerin admin panelinden duzenlenmesini saglayan class yapisi
    fields = ['name','slug','parent']


class CourseAdmin(admin.ModelAdmin):
    fields = ['courseName','course_isActive','courseLanguage','courseLevel','courseSlug',
              'courseDesc','coursePreVideo','courseCoverPhoto','courseStartDate','courseUpdateDate',
              'courseCategory','courseInstructor']


class CourseLevelsAdmin(admin.ModelAdmin):
    fields = ['courseLevel']


class CourseLanguagesAdmin(admin.ModelAdmin):
    fields = ['courseLanguage']

admin.site.register(Category,CategoryAdmin)
admin.site.register(CourseLanguage,CourseLanguagesAdmin)
admin.site.register(CourseLevels,CourseLevelsAdmin)
admin.site.register(Course,CourseAdmin)
