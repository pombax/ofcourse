from django.shortcuts import render,get_object_or_404
from .models import Category,Course,CourseLevels,CourseLanguage
from instructor.models import Lecture,Section
from ouser.models import OUser
from django.views.generic import ListView,DetailView
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import get_object_or_404,redirect
from django.http import Http404
from .filters import CourseFilter

# http://127.0.0.1/kurs/kurs-id
class CourseContent(DetailView):
    # course_detail.html dosyasinda {{ icerik }} olarak goruntulenecek
    context_object_name = "icerik"
    template_name = "courses/course_detail.html"

    # kullanicinin kursa kayitli olup olmadidigini donduren metod
    def is_enrolled(self,pk):
        ogrenci = OUser.objects.get(username__iexact=self.request.user.username)  #ogrenciyi getir
        flag = False
        for crs in ogrenci.courses.all():  #ogrencinin aldigi tum dersleri incele
            if int(crs.courseID) == int(pk):    # id ler esit ise bayragi true yap dondur
                flag = True
        return flag

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            if self.is_enrolled(self.kwargs['course_ID']):  #kullanici o derse kayitli ise o dersin icerik sayfasini
                return redirect('/kurs/'+self.kwargs['course_ID'] +'/icerik')               #goruntulesin
            else:
                self.object = self.get_object()
                return super(CourseContent, self).get(request, *args, **kwargs)
        else:
           self.object = self.get_object()
           return super(CourseContent, self).get(request, *args, **kwargs)

    def get_object(self):
        return Course.objects.filter(courseID__iexact=self.kwargs['course_ID'])[0]

    def get_context_data(self, **kwargs):
        context = super(CourseContent,self).get_context_data(**kwargs)
        return context


#http://127.0.0.1:8000/kurs/1/icerik/
class IcerikleriGoruntule(DetailView):
    context_object_name = "icerik"
    template_name = "courses/course_detail_.html"

    def get_object(self):
        ogrenci = OUser.objects.get(username__iexact=self.request.user.username)
        bayrak = False
        for crs in ogrenci.courses.all():
            if int(crs.courseID) == int(self.kwargs['course_ID']):
                bayrak = True
        if bayrak is True:
            return Lecture.objects.filter(course=self.kwargs['course_ID'])

    def get_context_data(self, **kwargs):
        context = super(IcerikleriGoruntule,self).get_context_data(**kwargs)
        context['bolumler'] = Section.objects.filter(course=self.kwargs['course_ID'])
        context['course_informations'] = Course.objects.filter(courseID=self.kwargs['course_ID'])[0]
        #context['lecture'] = Lecture.objects.filter(course=self.kwargs['course_ID'])[0]
        return context



#http://127.0.0.1:8000/kurs/1/icerik/ders
class IcerikGoruntule(DetailView):
    context_object_name = "icerik"
    template_name = "courses/content_detail.html"

    def get_object(self):
        ogrenci = OUser.objects.get(username__iexact=self.request.user.username)
        bayrak = False
        for crs in ogrenci.courses.all():
            if int(crs.courseID) == int(self.kwargs['course_ID']):
                bayrak = True
        if bayrak is True:
            return Lecture.objects.filter(slug=self.kwargs['slug'])



#http://127.0.0.1:8000/kurslar/
class CourseListView(ListView):
    model = Course
    template_name = "courses/course_list.html"

    def get_context_data(self, **kwargs):
        context = super(CourseListView,self).get_context_data(**kwargs)
        context['kategoriler'] = Category.objects.all()
        context['seviyeler'] = CourseLevels.objects.all()
        context['kurslar'] =  Course.objects.all().filter(course_isActive=True).order_by('courseStartDate')
        context['diller'] = CourseLanguage.objects.all()
        context['filter'] = CourseFilter(self.request.GET,queryset=Course.objects.all())

        return context


#ornek http://127.0.0.1:8000/kurslar/web
class CategoryListView(ListView):
    '''
        Belirli kategoriye ait kurslari listeler
        Ornek ofcourse.com/kurslar/web
        web kategorisindeki kurslari listeler.
    '''
    model = Course
    template_name = "courses/course_list.html"
    context_object_name = "icerik"

    def get_context_data(self, **kwargs):
        context = super(CategoryListView,self).get_context_data(**kwargs)
        context['kategoriler'] = Category.objects.all()
        context['seviyeler'] = CourseLevels.objects.all()
        context['kurslar'] =  Course.objects.all().filter(course_isActive=True,courseCategory=context['kategoriler']).order_by()
        context['diller'] = CourseLanguage.objects.all()
        return context



def enrollCourse(request,*args, **kwargs):
    ''' kursa katilmasini saglar'''
    if request.user.is_authenticated():
        n_kurs = Course.objects.get(courseID__iexact=kwargs['course_ID'])
        n_kurs = n_kurs.courseID
        ogrenci = get_object_or_404(OUser.objects.filter(username=request.user.username,is_student=True))
        print (n_kurs)
        ogrenci.courses.add(n_kurs)
        ogrenci.save()
        return HttpResponse('Kursa Katildin ..')       # duzenlenecek
    else:
        return redirect("/register")


def kurstanayril(request,*args, **kwargs):
    if request.user.is_authenticated():
        c = Course.objects.get(courseID=kwargs['course_ID'])
        o = OUser.objects.get(username=request.user.username).courses.remove(c)
        return HttpResponse("Kurstan Ayrildiniz...")
    else:
        return redirect("/")
