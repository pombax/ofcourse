# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-04-17 02:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0006_auto_20160416_1858'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='courselevels',
            options={'verbose_name': 'Kurs Seviyeleri', 'verbose_name_plural': 'Kurs Seviyeleri'},
        ),
        migrations.RenameField(
            model_name='courselevels',
            old_name='courseLebel',
            new_name='courseLevel',
        ),
    ]
