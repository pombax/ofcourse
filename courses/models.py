from __future__ import unicode_literals
from django.db import models
from django.utils.text import slugify
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Category(models.Model):
    cid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    slug = models.SlugField()
    parent = models.ForeignKey('self', blank=True, null=True, related_name="children")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


@python_2_unicode_compatible
class Course(models.Model):
    LEVEL_CHOICES = (
        ('Beginner','Beginner'),
        ('Intermediate','Intermediate'),
        ('Advanced','Advanced'),
    )
    courseID = models.AutoField(primary_key=True)
    course_isActive = models.BooleanField(default=False)
    courseName = models.CharField(max_length=200,verbose_name="Kurs Adi")
    courseSlug = models.SlugField()
    courseDesc = models.TextField()
    courseCoverPhoto = models.ImageField(upload_to='covers/',default='covers/default.jpg')
    coursePreVideo = models.URLField()
    courseStartDate = models.DateField(null=True,blank=True)
    courseUpdateDate = models.DateField(null=True,blank=True)
    courseLanguage = models.ForeignKey('courses.CourseLanguage',blank=True,null=True,verbose_name="Dil")
    courseLevel = models.ForeignKey('courses.CourseLevels',blank=True,null=True,verbose_name="Seviye")
    courseCategory = models.ForeignKey('courses.Category',blank=True,null=True,verbose_name="Kategori")
    courseInstructor = models.ForeignKey('instructor.Instructor',blank=True,null=True,default="")

    def save(self, *args, **kwargs):
        self.courseSlug = slugify(self.courseName)
        self.courseStartDate = timezone.now()
        self.courseUpdateDate = None
        super(Course, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Kurs'
        verbose_name_plural = 'Kurslar'

    def __str__(self):
        return self.courseName


@python_2_unicode_compatible
class CourseLanguage(models.Model):
    courseLanguage = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Kurs Dili'
        verbose_name_plural = 'Kurs Dilleri'

    def __str__(self):
        return self.courseLanguage


@python_2_unicode_compatible
class CourseLevels(models.Model):
    courseLevel = models.CharField(max_length=200)

    class Meta:
        verbose_name = 'Kurs Seviyeleri'
        verbose_name_plural = 'Kurs Seviyeleri'

    def __str__(self):
        return self.courseLevel
