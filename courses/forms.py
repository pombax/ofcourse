from django import forms
from .models import Course
from django.utils import timezone
from django.http import HttpResponse


class CreateCourseForm(forms.ModelForm):    # kurs olusturma formu
    courseName = forms.CharField()

    class Meta:
        model = Course
        fields = ['courseName']

    def form_valid(self, form):             # form gecerli ise
        cf = form.save(commit=False)
        cf.courseInstructor = self.request.user
        cf.courseStartDate = timezone.now()
        cf.save()
        return HttpResponse("Kursunuz olusturulmustur")