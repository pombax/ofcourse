import datetime
from .models import Question,Answer
from .forms import QuestionForm,AnswerForm
from django.shortcuts import get_object_or_404, render, render_to_response
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404, render, render_to_response
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import DetailView,CreateView,ListView,UpdateView,View,FormView
from courses.models import Course
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormMixin
from django.contrib.auth import get_user
from ouser.models import OUser
from django.utils import timezone


class BaslikAc(UpdateView):
    form_class = QuestionForm
    model = Question
    template_name = "forum/baslikac.html"
    success_url ="/"
    context_object_name = "forumIcerik"

    def get_context_data(self, **kwargs):
        context = super(BaslikAc, self).get_context_data(**kwargs)
        context['kurs_id'] = Course.objects.all().filter(courseID=self.kwargs['course_ID'])
        context['form'] = QuestionForm
        return context

    def get_object(self):
        return get_object_or_404(Course,courseID = self.kwargs['course_ID'])

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.course = Course.objects.get(courseID=self.kwargs['course_ID'])
        form.instance.pub_date = timezone.now()
        return super(BaslikAc, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        form = QuestionForm(request.POST, request.FILES)

        if form.is_valid():
            #self.object = self.get_object()
            #obj = form.save(commit=False)
            #obj.save()
            self.form_valid(form)
            return HttpResponse("Soru Gonderilmistir")

        else:
            self.object = self.get_object()
            print "olmadi xd"
            print form.errors
            context = super(BaslikAc, self).get_context_data(**kwargs)
            context['form'] = form
            return self.render_to_response(context=context)


class QA(ListView):
    model = Question
    template_name = "qa/list.html"
    def get_context_data(self, **kwargs):
        context = super(QA, self).get_context_data(**kwargs)
        context['lastquestions'] = Question.objects.filter(course=self.kwargs['course_ID'])
        return context



class SoruDetay(UpdateView):
    context_object_name = "question_details"
    template_name = "qa/ques_detail.html"
    form_class = AnswerForm
    success_url = '/'
    model = Answer

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.question = Question.objects.get(id=self.kwargs['question_ID'])
        form.instance.pub_date = timezone.now()
        return super(SoruDetay, self).form_valid(form)

    def get_object(self):
        return get_object_or_404(Question,id = self.kwargs['question_ID'])

    def get_context_data(self, **kwargs):
        context = super(SoruDetay,self).get_context_data(**kwargs)
        context['lastanswers'] = Answer.objects.all().filter(question=self.kwargs['question_ID'])
        context['form'] = AnswerForm
        return context

    def post(self, request, *args, **kwargs):
        form = AnswerForm(request.POST, request.FILES)

        if self.form_valid(form):
            self.object = self.get_object()
            obj = form.save(commit=False)
            obj.save()
            return HttpResponse("Yorumunuz Gonderildi")

        else:
            self.object = self.get_object()
            context = super(SoruDetay, self).get_context_data(**kwargs)
            print "olmadi"
            context['form'] = form
            print form.errors
            return self.render_to_response(context=context)


