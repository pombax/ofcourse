from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ouser.models import OUser
from django.conf import settings

class Question(models.Model):
    id = models.AutoField(primary_key=True)
    question_text = models.CharField(max_length=200)
    question_title = models.CharField(max_length=200,null=True)
    pub_date = models.DateTimeField('date published',default=timezone.now)
    closed = models.BooleanField(default=False)
    user = models.ForeignKey(OUser,null=True)
    course = models.ForeignKey('courses.course',null=True)

    def __str__(self):
        return self.question_title


class Answer(models.Model):
    question = models.ForeignKey(Question,null=True)
    answer_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published',null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,null=True)

    def __str__(self):
        return self.answer_text

class Voter(models.Model):
    user = models.ForeignKey(OUser)
    answer = models.ForeignKey(Answer)

class QVoter(models.Model):
    user = models.ForeignKey(OUser)
    question = models.ForeignKey(Question)

class Comment(models.Model):
    answer = models.ForeignKey(Answer)
    comment_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    user_data = models.ForeignKey(OUser)

    def __str__(self):
        return self.comment_text
