# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-05-23 18:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0005_auto_20160523_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='question_title',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
