from .models import Question,Answer
from ouser.models import OUser
from django import forms
from django.utils import timezone

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question_title','question_text',)

    def form_valid(self, form):  # form gecerli ise
        cf = form.save(commit=False)
        cf.user = self.request.user
        cf.save()


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields =('answer_text',)

