from django.contrib import admin
from .models import Question,Answer


class QuestionAdmin(admin.ModelAdmin):
    fields = ['question_text','question_title','pub_date','course','user']

class AnswerAdmin(admin.ModelAdmin):
    fields = ['question','answer_text','votes','pub_date','author']


admin.site.register(Question,QuestionAdmin)
admin.site.register(Answer,AnswerAdmin)
